
//den 1
var nazevMesta = document.querySelector('#nazevMesta');
var ikona = document.querySelector('#ikona');
var minTeplota = document.querySelector('#minTeplota');
var maxTeplota = document.querySelector('#maxTeplota');
var rychlostVetru = document.querySelector('#rychlostVetru');
var datum = document.querySelector('#datum');

//den 2
var nazevMesta1 = document.querySelector('#nazevMesta1');
var ikona1 = document.querySelector('#ikona1');
var minTeplota1 = document.querySelector('#minTeplota1');
var maxTeplota1 = document.querySelector('#maxTeplota1');
var rychlostVetru1 = document.querySelector('#rychlostVetru1');
var datum1 = document.querySelector('#datum1');

//den 3
var nazevMesta2 = document.querySelector('#nazevMesta2');
var ikona2 = document.querySelector('#ikona2');
var minTeplota2 = document.querySelector('#minTeplota2');
var maxTeplota2 = document.querySelector('#maxTeplota2');
var rychlostVetru2 = document.querySelector('#rychlostVetru2');
var datum2 = document.querySelector('#datum2');

//den 4
var nazevMesta3 = document.querySelector('#nazevMesta3');
var ikona3 = document.querySelector('#ikona3');
var minTeplota3 = document.querySelector('#minTeplota3');
var maxTeplota3 = document.querySelector('#maxTeplota3');
var rychlostVetru3 = document.querySelector('#rychlostVetru3');
var datum3 = document.querySelector('#datum3');

var userInput = document.querySelector('#userInput');
var predpovedBtn = document.querySelector('#predpovedBtn');
var historie1Btn = document.querySelector('#mesto1');
var historie2Btn = document.querySelector('#mesto2');
var historie3Btn = document.querySelector('#mesto3');

var mesto = document.querySelector('#mesto1');
var mesto1 = document.querySelector('#mesto1');
var mesto2 = document.querySelector('#mesto2');
var mesto3 = document.querySelector('#mesto3');

var mesto1Domovske = document.querySelector('#mesto1Domovske');
var mesto2Domovske = document.querySelector('#mesto2Domovske');
var mesto3Domovske = document.querySelector('#mesto3Domovske');

var domovskeMesto = document.querySelector('#domovskeMesto');


var historieP = [];
var pomocPole = [];
var historie = [];

userInput.value = "Prague";

historie = localStorage.value;
const city = historie.split(',');
var domovskeMestoText = document.querySelector('#domovskeMesto');
domovskeMestoText.innerHTML = city[city.length - 1];

// historie
if (localStorage == undefined) {
	historie.innerHTML = "";
}
else {
	historie = localStorage.value;
	mesto1.value = localStorage.value;
	mesto1.innerHTML = mesto1.value;
	pomocPole[1] = 'Vyhledané města';
	pomocPole[2] = "Vyhledané města";
	pomocPole[3] = "Vyhledané města";
	const mesta = historie.split(',');
	mesto1.innerHTML = mesta[0];
	mesto2.innerHTML = mesta[1];
	mesto3.innerHTML = mesta[2];
}

//*******************************************start****************************************
this.onload = function () {
	fetch("https://weatherbit-v1-mashape.p.rapidapi.com/forecast/daily?city=" + city[city.length - 1], {
		"method": "GET",
		"headers": {
			"x-rapidapi-key": "14119e7c9dmsh285e1095d143201p184683jsnde1c7ec6d4da",
			"x-rapidapi-host": "weatherbit-v1-mashape.p.rapidapi.com"
		}
	})
		.then(function (response) {
			console.log(response);
			let result = response.json();
			return result;

		})

		.then(function (result) {
			//den
			nazevMesta.innerHTML = result.city_name;
			document.getElementById('obr0').src = "/img/icons/" + result.data[0].weather.icon + ".png";
			minTeplota.innerHTML = result.data[0].low_temp + '°';
			maxTeplota.innerHTML = result.data[0].high_temp + '°';
			rychlostVetru.innerHTML = Math.floor(result.data[0].wind_spd) + ' m/s';
			datum.innerHTML = result.data[0].valid_date;

			//den1
			nazevMesta1.innerHTML = result.city_name;
			document.getElementById('obr1').src = "/img/icons/" + result.data[1].weather.icon + ".png";
			minTeplota1.innerHTML = result.data[1].low_temp + '°';
			maxTeplota1.innerHTML = result.data[1].high_temp + '°';
			rychlostVetru1.innerHTML = Math.floor(result.data[1].wind_spd) + ' m/s';
			datum1.innerHTML = result.data[1].valid_date;

			//den2
			nazevMesta2.innerHTML = result.city_name;
			document.getElementById('obr2').src = "/img/icons/" + result.data[2].weather.icon + ".png";
			minTeplota2.innerHTML = result.data[2].low_temp + '°';
			maxTeplota2.innerHTML = result.data[2].high_temp + '°';
			rychlostVetru2.innerHTML = Math.floor(result.data[2].wind_spd) + ' m/s';
			datum2.innerHTML = result.data[2].valid_date;

			//den3
			nazevMesta3.innerHTML = result.city_name;
			document.getElementById('obr3').src = "/img/icons/" + result.data[3].weather.icon + ".png";
			minTeplota3.innerHTML = result.data[3].low_temp + '°';
			maxTeplota3.innerHTML = result.data[3].high_temp + '°';
			rychlostVetru3.innerHTML = Math.floor(result.data[3].wind_spd) + ' m/s';
			datum3.innerHTML = result.data[3].valid_date;

		})
}

predpovedBtn.onclick = function () {
	fetch("https://weatherbit-v1-mashape.p.rapidapi.com/forecast/daily?city=" + userInput.value, {
		"method": "GET",
		"headers": {
			"x-rapidapi-key": "14119e7c9dmsh285e1095d143201p184683jsnde1c7ec6d4da",
			"x-rapidapi-host": "weatherbit-v1-mashape.p.rapidapi.com"
		}
	})
		.then(function (response) {
			console.log(response);
			let result = response.json();
			return result;

		})

		.then(function (result) {
			//den
			nazevMesta.innerHTML = result.city_name;
			document.getElementById('obr0').src = "/img/icons/" + result.data[0].weather.icon + ".png";
			minTeplota.innerHTML = result.data[0].low_temp + '°';
			maxTeplota.innerHTML = result.data[0].high_temp + '°';
			rychlostVetru.innerHTML = Math.floor(result.data[0].wind_spd) + ' m/s';
			datum.innerHTML = result.data[0].valid_date;

			//den1
			nazevMesta1.innerHTML = result.city_name;
			document.getElementById('obr1').src = "/img/icons/" + result.data[1].weather.icon + ".png";
			minTeplota1.innerHTML = result.data[1].low_temp + '°';
			maxTeplota1.innerHTML = result.data[1].high_temp + '°';
			rychlostVetru1.innerHTML = Math.floor(result.data[1].wind_spd) + ' m/s';
			datum1.innerHTML = result.data[1].valid_date;

			//den2
			nazevMesta2.innerHTML = result.city_name;
			document.getElementById('obr2').src = "/img/icons/" + result.data[2].weather.icon + ".png";
			minTeplota2.innerHTML = result.data[2].low_temp + '°';
			maxTeplota2.innerHTML = result.data[2].high_temp + '°';
			rychlostVetru2.innerHTML = Math.floor(result.data[2].wind_spd) + ' m/s';
			datum2.innerHTML = result.data[2].valid_date;

			//den3
			nazevMesta3.innerHTML = result.city_name;
			document.getElementById('obr3').src = "/img/icons/" + result.data[3].weather.icon + ".png";
			minTeplota3.innerHTML = result.data[3].low_temp + '°';
			maxTeplota3.innerHTML = result.data[3].high_temp + '°';
			rychlostVetru3.innerHTML = Math.floor(result.data[3].wind_spd) + ' m/s';
			datum3.innerHTML = result.data[3].valid_date;

			localStorage.value = (result.city_name + ',') + historie;
			historie = localStorage.value;

			const mesta = historie.split(',');

			mesto1.innerHTML = mesta[0];
			mesto2.innerHTML = mesta[1];
			mesto3.innerHTML = mesta[2];



		})


		.catch(err => {
			console.error(err);
		});

}

//******************************************** Tlacitko historie***********************************************
historie1Btn.onclick = function () {
	ZiskejElementy(mesto1)

}
historie2Btn.onclick = function () {
	ZiskejElementy(mesto2)

}
historie3Btn.onclick = function () {
	ZiskejElementy(mesto3)
}



function ZiskejElementy(mesto) {
	fetch("https://weatherbit-v1-mashape.p.rapidapi.com/forecast/daily?city=" + mesto.innerHTML, {
		"method": "GET",
		"headers": {
			"x-rapidapi-key": "14119e7c9dmsh285e1095d143201p184683jsnde1c7ec6d4da",
			"x-rapidapi-host": "weatherbit-v1-mashape.p.rapidapi.com"
		}
	})

		.then(function (response) {
			console.log(response);
			let result = response.json();
			return result;

		})

		.then(function (result) {
			//den
			nazevMesta.innerHTML = result.city_name;
			document.getElementById('obr0').src = "/img/icons/" + result.data[0].weather.icon + ".png";
			minTeplota.innerHTML = result.data[0].low_temp + '°';
			maxTeplota.innerHTML = result.data[0].high_temp + '°';
			rychlostVetru.innerHTML = Math.floor(result.data[0].wind_spd) + ' m/s';
			datum.innerHTML = result.data[0].valid_date;

			//den1
			nazevMesta1.innerHTML = result.city_name;
			document.getElementById('obr1').src = "/img/icons/" + result.data[1].weather.icon + ".png";
			minTeplota1.innerHTML = result.data[1].low_temp + '°';
			maxTeplota1.innerHTML = result.data[1].high_temp + '°';
			rychlostVetru1.innerHTML = Math.floor(result.data[1].wind_spd) + ' m/s';
			datum1.innerHTML = result.data[1].valid_date;

			//den2
			nazevMesta2.innerHTML = result.city_name;
			document.getElementById('obr2').src = "/img/icons/" + result.data[2].weather.icon + ".png";
			minTeplota2.innerHTML = result.data[2].low_temp + '°';
			maxTeplota2.innerHTML = result.data[2].high_temp + '°';
			rychlostVetru2.innerHTML = Math.floor(result.data[2].wind_spd) + ' m/s';
			datum2.innerHTML = result.data[2].valid_date;

			//den3
			nazevMesta3.innerHTML = result.city_name;
			document.getElementById('obr3').src = "/img/icons/" + result.data[3].weather.icon + ".png";
			minTeplota3.innerHTML = result.data[3].low_temp + '°';
			maxTeplota3.innerHTML = result.data[3].high_temp + '°';
			rychlostVetru3.innerHTML = Math.floor(result.data[3].wind_spd) + ' m/s';
			datum3.innerHTML = result.data[3].valid_date;


		})
		.catch(err => {
			console.error(err);
		});

}





